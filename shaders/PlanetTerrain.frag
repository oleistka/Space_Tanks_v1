#version 330 compatibility
uniform sampler2D texture;
uniform sampler2D planetTexture;
uniform float width;
uniform float height;

uniform vec3 lightDirection = vec3(-1, 0, 0);
uniform vec3 lightPos = vec3(1000, 400, 300);

vec3 ambient = vec3(0.2);
vec3 diffuse = vec3(1);

in vec4 position;

void main()
{	
	vec4 terrain = texture2D(texture, vec2((position.x + width) / (width * 2), (position.y +  height) / (height * 2)));
    if(terrain.x > 0.5)
	{
		gl_FragColor = texture2D(planetTexture, gl_TexCoord[0].xy);
	}
	else
	{
		gl_FragColor = vec4(0, 0, 0, 1);
	}
}
