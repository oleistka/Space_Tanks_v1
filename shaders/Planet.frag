#version 330 compatibility
uniform sampler2D texture;
uniform sampler2D normalTexture;
uniform float width;
uniform float height;

uniform vec3 lightPos;

vec3 ambient = vec3(0.2);
vec3 diffuse = vec3(1);

in vec4 position;

void main()
{
	vec3 color = texture2D(texture, gl_TexCoord[0].xy).rgb;
	
	if(color.r + color.g + color.b == 0)
	{
		gl_FragColor = vec4(color, 1);
	}
	else
	{
		vec3 normal = normalize(vec3(texture2D(normalTexture, vec2((position.x + width) / (width * 2), (height * 2 - (position.y +  height)) / (height * 2)))) * 2 - 1);
		vec3 lightDir = normalize(lightPos - position.xyz);
		gl_FragColor = vec4((ambient + diffuse * max(dot(normal, lightDir), 0.0)) * color, 1);
	}
}
