#version 330 compatibility
uniform float radius;
uniform vec2 middle;

in vec4 position;

void main()
{	
	gl_FragColor = vec4(normalize(vec3(position.x - middle.x, position.y - middle.y, sqrt(pow(radius, 2) - pow(length(position.xy - middle), 2)))), 1) * 0.5 + 0.5;
}