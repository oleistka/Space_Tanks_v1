package spaceTanks.gameLogic;

import spaceTanks.gameLogic.components.*;

public class GameEntity {
    public Component position;
    public Component mass;
    public Component velocity;
    public Component tankData;
    public Component projectileData;
    public Component planetData;
    public Component collider;
}
