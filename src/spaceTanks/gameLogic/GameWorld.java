package spaceTanks.gameLogic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

import javafx.geometry.Pos;
import org.jsfml.graphics.Color;

import spaceTanks.gameLogic.components.*;
import spaceTanks.utilities.Vector2f;

public class GameWorld {
    private int cPlayers; //liczba graczy
    private int currentPlayer = 0;
    boolean ifSimulation = false;
    private Terrain terrain;

    public ArrayList<GameEntity> listOfTanks = new ArrayList<GameEntity>();
    public ArrayList<GameEntity> listOfPlanets = new ArrayList<GameEntity>();
    public ArrayList<GameEntity> listOfProjectiles = new ArrayList<GameEntity>();
    public ArrayList<GameEntity> entities = new ArrayList<GameEntity>();
    private MapSize mapSize;

    public GameWorld(MapSize mapSize, int cPlayers) {
        this.mapSize = mapSize;
        this.cPlayers = cPlayers;
        terrain = new Terrain(mapSize.getX(), mapSize.getY());
        generatePlanets();
        generateTanks();
    }

    public int getCPlayers() {
        return cPlayers;
    }

    public int getPanelHeight() {
        return mapSize.getY();
    }

    public int getPanelWidth() {
        return mapSize.getX();
    }

    public Terrain getTerrain() {
        return terrain;
    }

    private void generatePlanets() {
        float r, posX, posY, m, density, area;
        boolean flag;
        for (int i = 0; i < cPlayers + 2; ++i) {
            do {
                flag = false;
                r = (float) ThreadLocalRandom.current().nextDouble(mapSize.getMinRadius(), mapSize.getMaxRadius());
                posX = (float) ThreadLocalRandom.current().nextDouble(-mapSize.getX() / 2 + r + 20, mapSize.getX() / 2 - r - 20);
                posY = (float) ThreadLocalRandom.current().nextDouble(-mapSize.getY() / 2 + r + 20, mapSize.getY() / 2 - r - 20);
                density = (float) ThreadLocalRandom.current().nextDouble(mapSize.getMinDensity(), mapSize.getMaxDensity());
                //area = ;
                m = (float) (Math.pow(r, 1.33) * density);

                //m = (float) ThreadLocalRandom.current().nextDouble(6500, 8000);

                for (int j = 0; j < i; ++j) {
                    float rr, xx, yy;
                    GameEntity tmpEntity = listOfPlanets.get(j);
                    rr = ((PlanetData) tmpEntity.planetData).getRadius();
                    xx = ((Position) tmpEntity.position).getPositionX();
                    yy = ((Position) tmpEntity.position).getPositionY();
                    float distance = (posX - xx) * (posX - xx) + (posY - yy) * (posY - yy);
                    distance = (float) Math.sqrt(distance);
                    if (distance < r + rr + 60) {
                        flag = true;
                        break;
                    }
                }

            } while (flag);

            terrain.createCircle((int) posX + mapSize.getX() / 2, (int) posY + mapSize.getY() / 2, r, Color.WHITE);

            GameEntity entity = EntityFactory.createPlanet();
            ((Mass) entity.mass).setMass(m);
            ((Position) entity.position).setPosition(posX, posY);
            ((PlanetData) entity.planetData).setRadius(r);
            ((Collider) entity.collider).setCollider(r);
            listOfPlanets.add(entity);
        }

    }

    private void generateTanks() {
        for (int i = 0; i < cPlayers; ++i) {
            GameEntity conPlanet = listOfPlanets.get(i);
            float angle;
            angle = ThreadLocalRandom.current().nextFloat() * 360;
            float vecX, vecY;
            float posX, posY;
            float rad = (angle * (float) Math.PI) / 180;
            vecX = (float) Math.cos(rad) * (((PlanetData) conPlanet.planetData).getRadius() + 5);
            vecY = (float) Math.sin(rad) * (((PlanetData) conPlanet.planetData).getRadius() + 5);
            posX = ((Position) conPlanet.position).getPositionX() + vecX;
            posY = ((Position) conPlanet.position).getPositionY() + vecY;

            GameEntity entity = EntityFactory.createTank();

            ((Position) entity.position).setPosition(posX, posY);
            ((TankData) entity.tankData).setTankData(angle, 100, conPlanet);
            ((Collider) entity.collider).setCollider(12);
            listOfTanks.add(entity);

        }
    }

    public void moveTank(boolean ifLeft, float deltaTime) {
        if (ifSimulation) return;
        float crrPosX;
        float crrPosY;
        float multiply = mapSize.getMultiply();


        GameEntity tank = listOfTanks.get(currentPlayer);
        float planetPosX = ((Position) ((TankData) tank.tankData).getTankPlanet().position).getPositionX();
        float planetPosY = ((Position) ((TankData) tank.tankData).getTankPlanet().position).getPositionY();
        float planetRadius = ((PlanetData) ((TankData) tank.tankData).getTankPlanet().planetData).getRadius();
        float tankAngle = ((TankData) tank.tankData).getAngle();
        float angle = (180 * (multiply / (planetRadius / 3.141592f)) * deltaTime);

        if (ifLeft) {
            tankAngle = (tankAngle + 360 - angle) % 360;
        } else {
            tankAngle = (tankAngle + angle) % 360;
        }

        float vecX, vecY;
        float rad = (tankAngle * (float) Math.PI) / 180;
        vecX = (float) Math.cos(rad) * (planetRadius + 5);
        vecY = (float) Math.sin(rad) * (planetRadius + 5);
        crrPosX = planetPosX + vecX;
        crrPosY = planetPosY + vecY;

        ((Position) tank.position).setPosition(crrPosX, crrPosY);
        ((TankData) tank.tankData).setAngle(tankAngle);
    }

    public void moveCannon(boolean ifLeft, float deltaTime) {
        if (ifSimulation) return;
        float angle = ((TankData) listOfTanks.get(currentPlayer).tankData).getCannonAngle();
        float multiply = 20;
        float newAngle = multiply * deltaTime;

        if (ifLeft) {
            if (angle - newAngle <= -90) {
                angle = -90;
            } else {
                angle -= newAngle;
            }
        } else {
            if (angle + newAngle >= 90) {
                angle = 90;
            } else {
                angle += newAngle;
            }
        }
        ((TankData) listOfTanks.get(currentPlayer).tankData).setCannonAngle(angle);
    }

    public void shoot() {
        if (ifSimulation) return;
        GameEntity tank = listOfTanks.get(currentPlayer);
        GameEntity projectile = EntityFactory.createProjectile();
        ((Mass) projectile.mass).setMass(75);
        float projectileAngle = ((TankData) tank.tankData).getAngle() + ((TankData) tank.tankData).getCannonAngle();
        Vector2f norm = new Vector2f(1, 0);
        norm.rotation(projectileAngle * (float) Math.PI / 180, new Vector2f());
        ((Velocity) projectile.velocity).setVelocity(250 * norm.getX(), 250 * norm.getY());
        ((Position) projectile.position).setPosition(((Position) tank.position).getPositionX() + 15 * norm.getX(), ((Position) tank.position).getPositionY() + 15 * norm.getY());
        ((Collider) projectile.collider).setCollider(2);
        ((ProjectileData) projectile.projectileData).setDamage(34);
        ((ProjectileData) projectile.projectileData).setAliveTime(20);
        listOfProjectiles.add(projectile);
        ifSimulation = true;
        currentPlayer = (currentPlayer + 1) % cPlayers;
    }

    public boolean checkEnd() {
        if (listOfTanks.size() < 2) return true;
        return false;
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }


    public void update(float deltaTime) {
        if (!ifSimulation) return;

        GameEntity projectile = listOfProjectiles.get(0);
        Vector2f lastPos = new Vector2f(((Position) projectile.position).getPositionX(), ((Position) projectile.position).getPositionY());

        ((ProjectileData) projectile.projectileData).setAliveTime(((ProjectileData) projectile.projectileData).getAliveTime() - deltaTime);
        if (((ProjectileData) projectile.projectileData).getAliveTime() <= 0) {
            ifSimulation = false;
            listOfProjectiles.remove(0);
            return;
        }
        Vector2f vec = new Vector2f();
        //wyliczenie sily
        for (GameEntity planet : listOfPlanets) {
            Vector2f distance = Vector2f.subtract(new Vector2f(((Position) planet.position).getPositionX(), ((Position) planet.position).getPositionY()),
                    new Vector2f(((Position) projectile.position).getPositionX(), ((Position) projectile.position).getPositionY()));
            float vecLength = distance.length();
            float fg = 250 * (((Mass) planet.mass).getMass() * ((Mass) projectile.mass).getMass()) / (vecLength * vecLength);
            distance.normalize();
            vec.add(distance.multiply(fg));
        }

        //wyliczenie
        ((Velocity) projectile.velocity).getVelocity().add(vec.multiply(deltaTime / ((Mass) projectile.mass).getMass()));
        vec = ((Velocity) projectile.velocity).getVelocity().multi(deltaTime);
        ((Position) projectile.position).setPosition(((Position) projectile.position).getPositionX() + vec.getX(),
                ((Position) projectile.position).getPositionY() + vec.getY());

        //sprawdzanie kolizji
        for (Iterator<GameEntity> it = listOfTanks.iterator(); it.hasNext(); ) {
            GameEntity tank = it.next();
            float distanceX = (((Position) tank.position).getPositionX() - ((Position) projectile.position).getPositionX());
            float distanceY = (((Position) tank.position).getPositionY() - ((Position) projectile.position).getPositionY());
            float distance = distanceX * distanceX + distanceY * distanceY;
            distance = (float) Math.sqrt(distance);
            if (distance <= ((Collider) tank.collider).getRadius() + ((Collider) projectile.collider).getRadius()) {
                int hp = ((TankData) tank.tankData).getHp() - ((ProjectileData) projectile.projectileData).getDamage();
                if (hp <= 0) {
                    cPlayers--;
                    currentPlayer = currentPlayer % cPlayers;
                    it.remove();
                } else {
                    ((TankData) tank.tankData).setHp(hp);
                }
                ifSimulation = false;
                listOfProjectiles.remove(0);
                return;
            }
        }

        Vector2f collisionPoint = terrain.checkLineCollision((int) lastPos.getX() + mapSize.getX() / 2, (int) lastPos.getY() + mapSize.getY() / 2, (int) ((Position) projectile.position).getPositionX() + mapSize.getX() / 2, (int) ((Position) projectile.position).getPositionY() + mapSize.getY() / 2);

        if (collisionPoint != null) {
            ifSimulation = false;
            terrain.createCircle((int) ((Position) projectile.position).getPositionX() + mapSize.getX() / 2, (int) ((Position) projectile.position).getPositionY() + mapSize.getY() / 2, 10, Color.BLACK);
            listOfProjectiles.remove(0);
            return;
        }

		/*for(Iterator<GameEntity> it = listOfPlanets.iterator(); it.hasNext();){
			GameEntity planet = it.next();
			float distanceX = (planet.position.getPositionX() - projectile.position.getPositionX());
			float distanceY = (planet.position.getPositionY() - projectile.position.getPositionY());
			float distance = distanceX*distanceX + distanceY*distanceY;
			distance = (float) Math.sqrt(distance);
			if(distance <= planet.collider.getRadius() + projectile.collider.getRadius()){
				ifSimulation = false;
				terrain.createCircle((int) projectile.position.getPositionX() + mapSize.getX() / 2, (int) projectile.position.getPositionY() + mapSize.getY() / 2, 10, Color.BLACK);
				listOfProjectiles.remove(0);
				return;
			}
		}*/

    }


}
